﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using OpenQA.Selenium.Chrome;

namespace SeleniumDEMO
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                //Demo.Test01();
                Demo.Test03();
                
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                Console.ReadKey();
                Console.ResetColor();
            }
        }
    }
}