﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace SeleniumDEMO
{
    public class Demo
    {
        public class ChromeOptionsWithPrefs : ChromeOptions
        {
            public Dictionary<string, object> prefs { get; set; }
        }

        public static void Test01()
        {
            try
            {
                // IMPORTANT
                // http://stackoverflow.com/questions/37693106/selenium-2-53-not-working-on-firefox-47
                // Unfortunately Selenium WebDriver 2.53.0 is not compatible with Firefox 47.
                // The WebDriver component which handles Firefox browsers (FirefoxDriver) will be discontinued.
                // As of version 3.0, Selenium WebDriver will have MarionetteDriver as the default running implementation for Firefox tests.

                // Create a new instance of the Firefox driver.
                // Note that it is wrapped in a using clause so that the browser is closed
                // and the webdriver is disposed (even in the face of exceptions).

                // Also note that the remainder of the code relies on the interface,
                // not the implementation.

                // Further note that other drivers (InternetExplorerDriver,
                // ChromeDriver, etc.) will require further configuration
                // before this example will work. See the wiki pages for the
                // individual drivers at http://code.google.com/p/selenium/wiki
                // for further information.
                using (IWebDriver driver = new ChromeDriver())
                {
                    //Notice navigation is slightly different than the Java version
                    //This is because 'get' is a keyword in C#
                    driver.Navigate().GoToUrl("http://www.google.com/");

                    // Find the text input element by its name
                    IWebElement query = driver.FindElement(By.Name("q"));

                    // Enter something to search for
                    query.SendKeys("Cheese");

                    // Now submit the form. WebDriver will find the form for us from the element
                    query.Submit();

                    // Google's search is rendered dynamically with JavaScript.
                    // Wait for the page to load, timeout after 10 seconds
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                    wait.Until(d => d.Title.StartsWith("cheese", StringComparison.OrdinalIgnoreCase));

                    // Should see: "Cheese - Google Search" (for an English locale)
                    Console.WriteLine("Page title is: " + driver.Title);
                    if (driver.Title.StartsWith("Cheese"))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("Ok");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                Console.ReadKey();
                Console.ResetColor();
            }
        }

        public static void PrintPageWithAutoIt()
        {
            try
            {
                var options = new ChromeOptionsWithPrefs {
                    prefs = new Dictionary<string, object>
                    {
                        {"intl.accept_languages", "nl"}
                    }
                };

                // http://stackoverflow.com/questions/26894071/chromedriver-selenium-automate-downloads

                using (IWebDriver driver = new ChromeDriver())
                {
                    //Notice navigation is slightly different than the Java version
                    //This is because 'get' is a keyword in C#
                    driver.Navigate().GoToUrl("http://printatestpage.com/");

                    // Find the text input element by its name
                    IWebElement query = driver.FindElement(By.Id("PrintBlackAndWhite"));

                    // Enter something to search for
                    query.Click();

                    // Google's search is rendered dynamically with JavaScript.
                    // Wait for the page to load, timeout after 10 seconds
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));

                    // Change Print Settings
                    wait.Until<IWebElement>(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"destination-settings\"]/div[2]/button")));
                    IWebElement e = driver.FindElement(By.XPath("//*[@id=\"destination-settings\"]/div[2]/button"));
                    e.Click();

                    // Select Save as PDF
                    wait.Until<IWebElement>(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"destination-search\"]/div/div[3]/div[2]/div/ul/li[1]/span/span[1]")));
                    e = driver.FindElement(By.XPath("//*[@id=\"destination-search\"]/div/div[3]/div[2]/div/ul/li[1]/span/span[1]"));
                    e.Click();

                    // Print
                    wait.Until<IWebElement>(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"print-header\"]/div/button[1]")));
                    e = driver.FindElement(By.XPath("//*[@id=\"print-header\"]/div/button[1]"));
                    e.Click();

                    //Console.WriteLine("Page title is: " + driver.Title);
                    //if (driver.Title.StartsWith("Cheese"))
                    //{
                    //    Console.ForegroundColor = ConsoleColor.Green;
                    //    Console.Write("Ok");
                    //}
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                Console.ReadKey();
                Console.ResetColor();
            }
        }

        internal static void Test03()
        {
            try
            {
                using (IWebDriver driver = new ChromeDriver())
                {
                    //Notice navigation is slightly different than the Java version
                    //This is because 'get' is a keyword in C#
                    
                    //driver.Navigate().GoToUrl("http://printatestpage.com/");
                    driver.Navigate().GoToUrl("http://localhost:51234/");

                    // Find the text input element by its name
                    IWebElement query = driver.FindElement(By.Id("PrintBlackAndWhite"));

                    // Enter something to search for
                    query.Click();

                    // Google's search is rendered dynamically with JavaScript.
                    // Wait for the page to load, timeout after 10 seconds
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));

                    Thread.Sleep(TimeSpan.FromSeconds(2));

                    var list = driver.WindowHandles;

                    driver.SwitchTo().Window(driver.WindowHandles[1]);

                    // Change Print Settings
                    wait.Until<IWebElement>(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"destination-settings\"]/div[2]/button")));
                    IWebElement e = driver.FindElement(By.XPath("//*[@id=\"destination-settings\"]/div[2]/button"));
                    e.Click();
                    //var js_script =
                    //    "document.evaluate(\"//*[@id=\"destination-settings\"]/div[2]/button\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click()";
                    //driver.ExecuteJavaScript(js_script);

                    // Select Save as PDF
                    wait.Until<IWebElement>(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"destination-search\"]/div/div[3]/div[2]/div/ul/li[1]/span/span[1]")));
                    e = driver.FindElement(By.XPath("//*[@id=\"destination-search\"]/div/div[3]/div[2]/div/ul/li[1]/span/span[1]"));
                    e.Click();
                    // document.evaluate("//*[@id=\"destination-search\"]/div/div[3]/div[2]/div/ul/li[4]/span/span[1]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click()
                    //js_script =
                    //    "document.evaluate(\"//*[@id=\"destination-search\"]/div/div[3]/div[2]/div/ul/li[4]/span/span[1]\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click()";
                    //driver.ExecuteJavaScript(js_script);

                    // Print
                    wait.Until<IWebElement>(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"print-header\"]/div/button[1]")));
                    e = driver.FindElement(By.XPath("//*[@id=\"print-header\"]/div/button[1]"));
                    e.Click();
                    //js_script =
                    //    "document.evaluate(\"//*[@id=\"destination-search\"]/div/div[3]/div[2]/div/ul/li[4]/span/span[1]\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click()";
                    //driver.ExecuteJavaScript(js_script);

                    

                    const string autoit_path = @"D:\Capacitación\4- Selenium\DEMO AutoIt\";
                    const string autoit_exe = "PrintPage2.exe";

                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    using (var process = Process.Start(Path.Combine(autoit_path, autoit_exe)))
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(2));
                    }

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("Ok");
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                Console.ResetColor();
            }
        }
    }
}