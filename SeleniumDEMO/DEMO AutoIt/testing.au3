#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         nacho

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------
#include <Date.au3>
#include <MsgBoxConstants.au3>


$EPOCH = "1970/01/01 00:00:00"
$NOW = _NowCalc()
$time = _DateDiff("s", $EPOCH, $NOW)
MsgBox(4096, "", $time)

#cs
; Get System DateTime to add it to file name
Local $aMyDate, $aMyTime
Local $tCur = _Date_Time_GetLocalTime()
;_DateTimeSplit("2005/01/01 14:30", $aMyDate, $aMyTime)
_DateTimeSplit(_Date_Time_SystemTimeToDateTimeStr($tCur), $aMyDate, $aMyTime)

;MemoWrite("Current date/time .: " & _Date_Time_SystemTimeToDateTimeStr($tCur))
MsgBox($MB_SYSTEMMODAL, "title",_Date_Time_GetLocalTime())
MsgBox($MB_SYSTEMMODAL, "title", _Date_Time_SystemTimeToDateTimeStr($tCur))

For $x = 1 To $aMyDate[0]
    MsgBox($MB_SYSTEMMODAL, $x, $aMyDate[$x])
Next
For $x = 1 To $aMyTime[0]
    MsgBox($MB_SYSTEMMODAL, $x, $aMyTime[$x])
Next


; Local $folder = "D:\Temp\"
; Local $fileName = "test"
; Local $fileExtension = ".pdf"
; Local $v
; MsgBox($MB_SYSTEMMODAL, '', "The Date is: " & _NowDate())
; MsgBox($MB_SYSTEMMODAL, "Pc Long format", _DateTimeFormat(_NowCalc(), 1))
;MsgBox($MB_SYSTEMMODAL, "Pc Short format", _DateTimeFormat(_NowCalc(), 4))

; ControlSetText("Guardar como", "", "[CLASS:Edit; INSTANCE:1]", "D:\Temp\test.pdf");
; ControlClick("Guardar como", "", "[CLASS:Button; INSTANCE:1]")
;MsgBox(64, "Mensaje", "Hecho - Se cierra en 10 segundos", 10)


Local $aMyDate, $aMyTime
Local $tCur = _Date_Time_GetLocalTime()
;_DateTimeSplit("2005/01/01 14:30", $aMyDate, $aMyTime)
;_DateTimeSplit(_Date_Time_GetLocalTime(), $aMyDate, $aMyTime)
_DateTimeSplit(_Date_Time_SystemTimeToDateTimeStr($tCur), $aMyDate, $aMyTime)

;MemoWrite("Current date/time .: " & _Date_Time_SystemTimeToDateTimeStr($tCur))
MsgBox($MB_SYSTEMMODAL, "title",_Date_Time_GetLocalTime())
MsgBox($MB_SYSTEMMODAL, "title", _Date_Time_SystemTimeToDateTimeStr($tCur))

For $x = 1 To $aMyDate[0]
    MsgBox($MB_SYSTEMMODAL, $x, $aMyDate[$x])
Next
For $x = 1 To $aMyTime[0]
    MsgBox($MB_SYSTEMMODAL, $x, $aMyTime[$x])
Next
#ce