#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         nacho

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------
#include <Date.au3>
#include <MsgBoxConstants.au3>
#include <GUIConstants.au3>


; Get timestamp to add it to file name
$EPOCH = "1970/01/01 00:00:00"
$NOW = _NowCalc()
$timestamp = _DateDiff("s", $EPOCH, $NOW)
; MsgBox(4096, "", $time)
Local $timestamp

; Local $folder = "D:\Temp\"
Local $folder = "D:\Capacitación\4- Selenium\DEMO AutoIt\"
Local $fileName = "test"
Local $ext = ".pdf"

Local $fileFullName = $folder & $fileName & "_" & $timestamp & $ext
; MsgBox(4096, "", $fileFullName)

ControlSetText("Guardar como", "", "[CLASS:Edit; INSTANCE:1]", $fileFullName);
ControlClick("Guardar como", "", "[CLASS:Button; INSTANCE:1]")


;MsgBox(64, "Mensaje", "Hecho! - Se cierra en 10 segundos", 10)

Local $Form1 = GUICreate("Hecho!", 333, 128, 200, 125)
Local $Button1 = GUICtrlCreateButton("OK", 128, 80, 75, 25)
Local $Label = GUICtrlCreateLabel("", 50, 32, 200, 30, $SS_CENTER)
Local $msg = ""
;GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
GUISetState(@SW_SHOW)

While 1
    For $i = 5 to 1 Step -1
	  $msg = "Hecho! - Se cierra en " & $i & " segundos"
	  GUICtrlSetData($Label, $msg)
	  Sleep(1000)
    Next

    GUICtrlSetData($Label, "Adios")
    Sleep(1000)
    ExitLoop
WEnd
Exit